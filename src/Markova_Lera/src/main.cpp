#include "tpolinom.h"
#include <iostream>

using namespace std;

int main()
{
    int choice;
    double x, y, z;
    TPolinom Pol_1; // 1-�� �������
    TPolinom Pol_2; // 2-�� �������
    cout << "Please enter a polynomial in the form `2x^1y^2z^3-3x^4y^5z^6`" << endl;
    cout << "Enter first polynom" << endl;
    cin >> Pol_1;
    cout << "Enter second polynom" << endl;
    cin >> Pol_2;
    cout << "Select action: " << endl
        << "1. Summarize polynomials" << endl
        << "2. Calculate first polynom" << endl
        << "3. Calculate second polynom" << endl
        << "4. Exit programm" << endl;
    cin >> choice;
    switch (choice)
    {
    case 1:
        cout << (Pol_1 + Pol_2) << endl;
        break;
    case 2:
        cout << "Enter the values of x, y, z: " << endl;
        cin >> x >> y >> z;
        cout << Pol_1.Calc(x, y, z) << endl;
        break;
    case 3:
        cout << "Enter the values of x, y, z: " << endl;
        cin >> x >> y >> z;
        cout << Pol_2.Calc(x, y, z) << endl;
        break;
    default:
        cout << "Error";
        break;
    }
}