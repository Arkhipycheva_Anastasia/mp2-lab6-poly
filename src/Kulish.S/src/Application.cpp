#include "Application.h"

void Application::setPOne() {
  system("cls");
  cout << "Enter first polynom" << endl;
  cin >> polyOne;
  flagPO = true;
}

void Application::setPTwo() {
  system("cls");
  cout << "Enter second polynom" << endl;
  cin >> polyTwo;
  flagPT = true;
}

void Application::help() {
  while (true) {
    choice = NULL;
    system("cls");
    cout << "The polynomials are entered without spaces, dividing monoms by operations" << endl;
    cout << " Polynomials degree should be at less or equal to 9" << endl;
    cout << "Operations are:" << endl;
    cout << "Addition, subtraction" << endl<<endl;
    
    cout << "Example:" << endl;
    cout << "3x^2y^1z^3+5x^1y^0z^3-8x^7y^2z^3" << endl<<endl;
    
    cout << "Press any key to exit help" << endl;

    choice = getche();
    if (choice != NULL)
      break;
  }

}

void Application::derivative() {
  system("cls");
  choice = NULL;
  cout << "Choose option" << endl;
  cout << "1) Derivative first polynom by X" << endl;
  cout << "2) Derivative first polynom by Y" << endl;
  cout << "3) Derivative first polynom by Z" << endl;
  cout << "4) Derivative second polynom by X" << endl;
  cout << "5) Derivative second polynom by Y" << endl;
  cout << "6) Derivative second polynom by Z" << endl;

  choice = getche();

  switch (choice)
  {
  case '1': {
    if (flagPO) {
      cout << endl << "Derivative of first polynom by X = " << polyOne.derivative(x) << endl;
      choice = getche();
    } else {
      setPOne();
      cout << endl << "Derivative of first polynom by X = " << polyOne.derivative(x) << endl;
      choice = getche();
    }
  } break;
  case '2': {
    if (flagPO) {
      cout << endl << "Derivative of first polynom by Y = " << polyOne.derivative(y) << endl;
      choice = getche();
    } else {
      setPOne();
      cout << endl << "Derivative of first polynom by Y = " << polyOne.derivative(y) << endl;
      choice = getche();
    }
  } break;
  case '3': {
    if (flagPO) {
      cout << endl << "Derivative of first polynom by Z = " << polyOne.derivative(z) << endl;
      choice = getche();
    } else {
      setPOne();
      cout << endl << "Derivative of first polynom by Z = " << polyOne.derivative(z) << endl;
      choice = getche();
    }
  } break;
  case '4': { if (flagPT) {
    cout << endl << "Derivative of second polynom by X = " << polyTwo.derivative(x) << endl;
    choice = getche();
  } else {
       setPTwo();
       cout << endl << "Derivative of second polynom by X = " << polyTwo.derivative(x) << endl;
       choice = getche();
    }
  } break;
  case '5': {
    if (flagPT) {
      cout << endl << "Derivative of second polynom by Y = " << polyTwo.derivative(y) << endl;
      choice = getche();
    } else {
      setPTwo();
      cout << endl << "Derivative of second polynom by Y = " << polyTwo.derivative(y) << endl;
      choice = getche();
    }
  } break;
  case '6': { if (flagPT) {
    cout << endl << "Derivative of second polynom by Z = " << polyTwo.derivative(z) << endl;
    choice = getche();
  } else {
      setPTwo();
      cout << endl << "Derivative of second polynom by Z = " << polyTwo.derivative(z) << endl;
      choice = getche();
    }
  } break;
 }
}

void Application::menu() {
  while (true) {
    choice = NULL;
    system("cls");
    cout << "Choose option" << endl;
    cout << "1) Help" << endl;
    cout << "2) Enter first polynom" << endl;
    cout << "3) Enter serond polynom" << endl;
    cout << "4) Find derivative" << endl;
    cout << "5) Fold polynoms" << endl;
    cout << "6) Exit" << endl;
    choice = getche();

    if (choice!=NULL)
      switch (choice) {
      case '1': help(); break;
      case '2': setPOne(); break;
      case '3': setPTwo(); break;
      case '4': derivative(); break;
      case '5': { if (flagPO&&flagPT) {

          choice = NULL;
          cout << "Summ of your polynoms = ";
          cout << (polyOne + polyTwo) << endl<<endl;
          cout << "Press any key to continue" << endl;
          choice = getche();

      } else { if (!flagPO&&!flagPT) {

                    setPOne();
                    setPTwo();
                    choice = NULL;
                    system("cls");
                    cout << polyOne << " + " << polyTwo << " = " << (polyOne + polyTwo) << endl << endl;
                    cout << "Press any key to continue" << endl;
                    choice = getche();

                  } else if (!flagPO) {

                    setPOne();
                    choice = NULL;
                    system("cls");
                    cout << polyOne << " + " << polyTwo << " = " << (polyOne + polyTwo) << endl << endl;
                    cout << "Press any key to continue" << endl;
                    choice = getche();

                  } else if (!flagPT) {

                    setPTwo();
                    choice = NULL;
                    system("cls");
                    cout << polyOne<<" + "<<polyTwo<<" = "<< (polyOne + polyTwo) << endl << endl;
                    cout << "Press any key to continue" << endl;
                    choice = getche();
                  }
                }
      } break;
      case '6': exitApp = true; break;
      }
    if (exitApp) break;
  }
}
