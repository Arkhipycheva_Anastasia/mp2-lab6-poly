#include "gtest/gtest.h"
#include "TDatList.h"

// Testing type (int)
typedef struct ValueInt:TDatValue {
	int Value;
	ValueInt() { Value = 0; }
	virtual TDatValue * getCopy() { TDatValue *temp = new ValueInt; return temp; }
} *PValueInt;

// Initialization
class TestList : public ::testing::Test
{
protected:
	virtual void SetUp()
	{
		// Testing only elem
		Val = new ValueInt();
		Val->Value = 100;

		// Testing ten elems 
		ArrVal = new PValueInt[N];
		for (int i = 0; i < N; i++)
		{
			ArrVal[i] = new ValueInt();
			ArrVal[i]->Value = i;
		}
	}
	virtual void TearDown()
	{
		delete ArrVal;
	}
	// Arrange
	const int N = 10;
	PValueInt Val;
	PValueInt *ArrVal;
	TDatList List;
};

TEST_F(TestList, Can_Add_An_Item_To_The_End) {
	// Arg
	List.insLast(Val);

	// Assert
	PValueInt temp = (PValueInt)List.getDatValue();
	EXPECT_EQ(Val->Value,temp->Value);
}

TEST_F(TestList, Can_Get_Size_Of_List) {
	// Arg
	for (int i = 0; i < N; i++)
		List.insLast(ArrVal[i]);

	// Assert
	EXPECT_EQ(List.getListLength(), N);
}

TEST_F(TestList, Can_Add_Ten_Elems_To_The_End_On_List) {
	// Arg
	for (int i = 0; i < N; i++)
		List.insLast(ArrVal[i]);

	// Assert
	for (int i = 0; i < N; i++, List.goNext())
		EXPECT_EQ( ((PValueInt)List.getDatValue())->Value, ArrVal[i]->Value );
}

TEST_F(TestList, can_add_ten_elems_to_the_first_on_list) {
	// Arg
	for (int i = 0; i < N; i++)
		List.insFirst(ArrVal[i]);

	// Assert
	for (int i = N-1; i > 0; i--, List.goNext())
		EXPECT_EQ( ((PValueInt)List.getDatValue())->Value, ArrVal[i]->Value );
}

TEST_F(TestList, Can_Add_Ten_Elems_To_The_Current_On_List) {
	// Arg
	for (int i = 0; i < 5; i++)
		List.insLast(ArrVal[i]);
	// 0 1 2 3 4
	// Current = 0
	List.goNext();
	List.goNext();
	// Current = 2
		List.insCurrent(Val);
	// 0 1 100 2 3 4
	
	// Assert
	List.reset();
	EXPECT_EQ(((PValueInt)List.getDatValue())->Value, ArrVal[0]->Value);
	List.goNext();
	EXPECT_EQ(((PValueInt)List.getDatValue())->Value, ArrVal[1]->Value);
	List.goNext();
	EXPECT_EQ(((PValueInt)List.getDatValue())->Value, Val->Value);
	List.goNext();
	EXPECT_EQ(((PValueInt)List.getDatValue())->Value, ArrVal[2]->Value);
}

TEST_F(TestList, Can_Delete_Elem_To_The_Current_On_List) {
	// Arg
	for (int i = 0; i < N; i++)
		List.insLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8 9
	List.goNext();
	List.delCurrent();
	// 0 2 3 4 5 6 7 8 9

	// Assert
	EXPECT_EQ(((PValueInt)List.getDatValue())->Value, ArrVal[2]->Value);
	EXPECT_EQ(List.getListLength(), N-1);
}

TEST_F(TestList, Delete_Elem_To_The_First_On_List) {
	// Arg
	for (int i = 0; i < N; i++)
		List.insLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8 9
	List.delFirst();
	// 1 2 3 4 5 6 7 8 9

	// Assert
	EXPECT_EQ(((PValueInt)List.getDatValue())->Value, ArrVal[1]->Value);
	EXPECT_EQ(List.getListLength(), N - 1);
}