#pragma once

class TDatValue 
{
public:
	virtual TDatValue * getCopy() = 0; // creating copy

	~TDatValue() {}
};	
typedef TDatValue *PTDatValue;