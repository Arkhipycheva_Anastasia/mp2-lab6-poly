#pragma once
#include "TPolynom.h"
#include <conio.h>
class Application
{
 public:
    void menu();
    
 private:
   char choice;
   bool flagPO = false;
   bool flagPT = false;
   bool exitApp = false;

   TPolynom polyOne;
   TPolynom polyTwo;

   void help();

   void derivative();
   void setPOne();
   void setPTwo();

};