//Copyright 2016 Ovcharuk Oleg
#pragma once

#include "TPolinom.h"
#include <iostream>

using namespace std;

class Menu {
private:
	int choise=0;
	TPolinom A;
	TPolinom B;
	int _x, _y, _z = 0;
	char variable;

	void rules(void){
		char mindgames = 'N';
		cout << "������������, ��� ���������� ��������� ��� ����������� ������ � ����������. " << endl
			<< "��� ����� ���������� ������, ����������, ������������ � ��������� " << endl
			<< "��������� ����� ��������: " << endl
			<< "1)������� �������� ��� �������� � ��� ������ ��������;" << endl
			<< "2)������ ����� ������� ������ �����: ax^0y^0z^0, ��� a-�����������, " << endl
			<< "� ������ ����� ����� ������ ����� ������� (0<=n<=9). ����� ��������, ��� ����" << endl
			<< "������� ������� ����������� �����������. " << endl
			<< "������ ��������� ���������� ��������: " << endl
			<< "3x^2y^0z^3-12x^1y^1z^3+4x^3y^2z^1" << endl;
		while (mindgames != 'Y'){
			cout << "�� ����������� ������������ � ���������? (Y/N) ";
			cin >> mindgames;
		}
	}

	void setA(void) {
		cout << "������� �������: ";
		cin >> A;
	}

	void setB(void) {
		cout << "������� ������ �������: ";
		cin >> B;
	}

	void buffer(void){
		system("cls");
		cout << "��� �������: " << A;
	}

	void pause(void){
		char p = 'N';
		while (p != 'Y'){
			cout << "���������� ������?(Y/N) ";
			cin >> p;
		}


	}

	void selection(void){
		cout << "��������, ��� �� ������ �������: " << endl
			<< "1) ������ ������� ������;" << endl
			<< "2) ������������������� �������;" << endl
			<< "3) ���������������� �������;" << endl
			<< "4) ��������� ��������;" << endl
			<< "5) ������� � ������ ���������;" << endl
			<< "6) ��������� ������." << endl
			<< "��� �����: ";
	}

public:
	
	void start(void){
		rules();
		system("cls");
		setA();
		while (choise != 6){
			buffer();
			selection();
			cin >> choise;
			switch (choise){
			case 1:
				setA();
				break;
			case 2:
				cout << "�������, �� ����� ���������� ��������������(x/y/z): ";
				cin >> variable;
				switch (variable){
					case 'x': cout << A.Diff(x); break;
					case 'y': cout << A.Diff(y); break;
					case 'z': cout << A.Diff(z); break;
					default: cout << "Error!" << endl; break;
				}
				break;
			case 3:
				cout << "�������, �� ����� ���������� �����������(x/y/z): ";
				cin >> variable;
				switch (variable){
					case 'x': cout << A.Integr(x); break;
					case 'y': cout << A.Integr(y); break;
					case 'z': cout << A.Integr(z); break;
					default: cout << "Error!" << endl; break;
				}
				break;
			case 4:
				cout << "������� x: ";
				cin >> _x;
				cout << "������� y: ";
				cin >> _y;
				cout << "������� z: ";
				cin >> _z;
				cout << "�������� �������� ��� �������� ��������� ���������� = "<< endl
					<< A.Calc(_x, _y, _z);
				break;
			case 5:
				setB();
				cout <<"����� ���������: "<< endl
					<< A + B;
				break;
			case 6:
				cout << "����� �������!" << endl;
				break;
			default:
				cout << "Error! " << endl;
				break;
			}
			pause();
		}
	}

};