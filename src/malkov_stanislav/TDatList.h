#pragma once

#include <string>
#include "TDatLink.h"
#include "TDatValue.h"

enum TLinkPos {FIRST, CURRENT, LAST};

class TDatList {
protected:
    PTDatLink pFirst;    // ������ �����
    PTDatLink pLast;     // ��������� �����
    PTDatLink pCurrLink; // ������� �����
    PTDatLink pPrevLink; // ����� ����� �������
    PTDatLink pStop;     // �������� ���������, ����������� ����� ������ 
    int CurrPos;         // ����� �������� ����� (��������� �� 0)
    int ListLen;         // ���������� ������� � ������

    PTDatLink GetLink(int pos) const {
        if (pos < 0 || pos >= GetListLength())
            throw std::string("Position out of range!");
        PTDatLink link = pFirst;
        for (int i = 0; i < pos; i++)
            link = link->GetNextDatLink();
        return link;
    };

    void DelLink(PTDatLink pLink) {
        Reset();
        while (pCurrLink != pLink && !IsListEnded())
            GoNext();
        if (IsListEnded())
            return;
        else {
            DelCurrent();
            Reset();
        }
    };

public:
    TDatList() {
        pStop = nullptr;
        pFirst = nullptr;
        pLast = nullptr;
        pCurrLink = nullptr;
        pPrevLink = nullptr;
        CurrPos = 0;
        ListLen = 0;
    }

    ~TDatList() {
        DelList();
    }

    PTDatValue GetDatValue(TLinkPos mode = CURRENT) const {
        try {
            switch (mode) {
            case CURRENT:
                return pCurrLink->GetDatValue();
            case FIRST:
                return pFirst->GetDatValue();
            case LAST:
                return pLast->GetDatValue();
            default:
                return nullptr;
            }
        }
        catch (...) {
            throw std::string("Invalid GetDatValue mode!");
        }
    };

    virtual int IsEmpty() const { 
        return pFirst == pStop; 
    }

    int GetListLength() const {
        return ListLen;
    }

    int SetCurrentPos(int pos) {
        if (pos < 0 || pos >= GetListLength())
            throw std::string("Position out of range!");
        PTDatLink curLink = pFirst;
        for (int i = 0; i < pos - 1; i++)
            curLink = curLink->GetNextDatLink();
        pPrevLink = curLink;
        pCurrLink = curLink->GetNextDatLink();
        CurrPos = pos;
        return 0;
    };

    int GetCurrentPos(void) const {
        return CurrPos;
    };

    virtual int Reset(void) {
        CurrPos = 0;
        pCurrLink = pFirst;
        pPrevLink = nullptr;
        return 0;
    }; 

    virtual int IsListEnded(void) const {
        return pCurrLink == pStop;
    }

    int GoNext() {
        if (IsListEnded() || !GetListLength())
            return 1;
        else {
            pPrevLink = pCurrLink;
            pCurrLink = pCurrLink->GetNextDatLink();
            return IsListEnded();
        }
    }

    virtual void InsFirst(PTDatValue pVal = nullptr) {
        PTDatLink link = new TDatLink(pVal, pFirst);
        pFirst = link;
        if (pLast == nullptr) {
            pLast = pFirst;
            pLast->SetNextLink(pStop);
        }
        if (pCurrLink == nullptr)
            pCurrLink = pFirst;
        ListLen++;
    }

    virtual void InsLast(PTDatValue pVal = nullptr) {
        PTDatLink link = new TDatLink(pVal, pStop);
        link->SetDatValue(pVal);
        if (pLast != nullptr) {
            pLast->SetNextLink(link);
        }
        pLast = link;
        if (pFirst == nullptr)
            pFirst = pLast;
        if (pCurrLink == nullptr)
            pCurrLink = pFirst;
        ListLen++;
    }

    virtual void InsCurrent(PTDatValue pVal = nullptr) {
        // ���� ������ ������ ���� ��� ������� ������� ������
        if (pCurrLink == nullptr || pPrevLink == nullptr) {
            InsFirst(pVal);
        }
        else {
            PTDatLink link = new TDatLink(pVal, pCurrLink);
            pPrevLink->SetNextLink(link);
            link->SetDatValue(pVal);
            CurrPos++;
        }
        ListLen++;
    }

    virtual void DelFirst(void) {
        if (pFirst != nullptr) {
            if (pFirst == pCurrLink)
                pCurrLink = pFirst->GetNextDatLink();
            if (pFirst == pLast)
                pLast = nullptr;
            PTDatLink link = pFirst;
            pFirst = pFirst->GetNextDatLink();
            delete link;
            ListLen--;
        }
    }

    virtual void DelCurrent(void) {
        if (pCurrLink != nullptr) {
            if (pPrevLink != nullptr) {
                if (pCurrLink == pFirst)
                    pFirst = pFirst->GetNextDatLink();
                if (pCurrLink == pLast)
                    pLast = pPrevLink;
                PTDatLink link = pCurrLink;
                pCurrLink = pCurrLink->GetNextDatLink();
                pPrevLink->SetNextLink(pCurrLink);
                delete link;
                ListLen--;
            }
            else
                DelFirst();
        }
    }

    virtual void DelList(void) {
        if (pFirst == nullptr)
            return;
        PTDatLink link = pFirst;
        while (link->GetNextDatLink() != pStop) {
            PTDatLink delLink = link;
            link = link->GetNextDatLink();
            delete delLink;
        }
        pFirst = nullptr;
        pLast = nullptr;
        pCurrLink = nullptr;
        pPrevLink = nullptr;
        ListLen = 0;
    }
};
