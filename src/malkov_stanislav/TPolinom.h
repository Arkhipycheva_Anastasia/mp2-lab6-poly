#pragma once

#include <iostream>
#include "THeadRing.h"
#include "TMonom.h"

class TPolinom : public THeadRing {
public:
    TPolinom(int monoms[][2] = nullptr, int km = 0) {
        pHead->SetDatValue(new TMonom(0, -1));
        for (int i = 0; i < km; i++)
            InsLast(new TMonom(monoms[i][0], monoms[i][1]));
    }

    TPolinom(const TPolinom &q) {
        pHead->SetDatValue(new TMonom(0, -1));
        for (int i = 0; i < q.GetListLength(); i++)
            InsLast(q.GetLink(i)->GetDatValue()->GetCopy());
        ListLen = q.ListLen;
    }

    PTMonom  GetMonom() const { 
        return (PTMonom)GetDatValue(); 
    }

    TPolinom & operator+(TPolinom &q) {
        TPolinom* p = new TPolinom(*this);
        q.Reset();
        while (!q.IsListEnded()) {
            PTMonom monom = q.GetMonom();
            bool containsThatIndex = false;
            if(p->pFirst != nullptr)
                for (PTDatLink link = p->pFirst; link != p->pHead; link = link->GetNextDatLink()) {
                    PTMonom this_monom = (PTMonom)link->GetDatValue();
                    if (this_monom->GetIndex() == monom->GetIndex()) {
                        this_monom->SetCoeff(this_monom->GetCoeff() + monom->GetCoeff());
                        containsThatIndex = true;
                        break;
                    }
                }
            if (!containsThatIndex)
                p->InsLast(monom->GetCopy());
            q.GoNext();
        }
        p->CheckForEmptyMonoms();
        return *p;
    }

    TPolinom & operator-(TPolinom &q) {
        TPolinom* p = new TPolinom(*this);
        q.Reset();
        while (!q.IsListEnded()) {
            PTMonom monom = q.GetMonom();
            bool containsThatIndex = false;
            if(p->pFirst != nullptr)
                for (PTDatLink link = p->pFirst; link != p->pHead; link = link->GetNextDatLink()) {
                    PTMonom this_monom = (PTMonom)link->GetDatValue();
                    if (this_monom->GetIndex() == monom->GetIndex()) {
                        this_monom->SetCoeff(this_monom->GetCoeff() - monom->GetCoeff());
                        containsThatIndex = true;
                        break;
                    }
                }
            if (!containsThatIndex) {
                PTMonom m = (PTMonom)monom->GetCopy();
                m->SetCoeff(-m->GetCoeff());
                p->InsLast(m);
            }
            q.GoNext();
        }
        p->CheckForEmptyMonoms();
        return *p;
    }

    TPolinom & operator*(TPolinom &q) {
        TPolinom* p_new = new TPolinom(*this);
        TPolinom* p2 = &q;
        Reset();
        p_new->DelList();
        while (!IsListEnded()) {
            p2->Reset();
            PTMonom m1 = GetMonom();
            while (!p2->IsListEnded()) {
                PTMonom m2 = p2->GetMonom();
                PTMonom monom = new TMonom(m1->GetCoeff() * m2->GetCoeff(), m1->GetIndex() + m2->GetIndex());
                TPolinom tmp;
                tmp.InsFirst(monom);
                *p_new = (*p_new)+ tmp;
                p2->GoNext();
            }
            GoNext();
        }
        p_new->CheckForEmptyMonoms();
        return *p_new;
    }

    TPolinom & operator=(TPolinom &q) {
        TPolinom temp = TPolinom(q);
        DelList();
        temp.Reset();
        while (!temp.IsListEnded()) {
            InsLast(temp.GetMonom()->GetCopy());
            temp.GoNext();
        }
        Reset();
        return *this;
    }

    double Calculate(double x, double y = 0, double z = 0) {
        if (IsEmpty())
            return 0;
        double result = 0;
        Reset();
        while (!IsListEnded()) {
            PTMonom monom = GetMonom();
            double monomResult = 0;
            if(monom->GetX())
                monomResult += pow(x, monom->GetX());
            if(monom->GetY())
                monomResult += pow(y, monom->GetY());
            if(monom->GetZ()) 
                monomResult += pow(z, monom->GetZ());
            monomResult *= monom->GetCoeff();
            result += monomResult;
            GoNext();
        }
        return result;
    }

    void Print() {
        if (IsEmpty())
            return;
        for (PTDatLink link = pFirst; link != pHead; link = link->GetNextDatLink()) {
            PTMonom monom = (PTMonom)link->GetDatValue();
            std::cout << monom->GetCoeff() << "*x^" << monom->GetX() << "*y^" << monom->GetY() << "*z^" << monom->GetZ() << " + ";
        }
    }

    void PrintInfo() {
        std::cout << "INFO: " << pFirst << " " << pLast << " " << pCurrLink << " " << pHead << " " << ListLen << std::endl;
        getchar();
    }
 private:
    void CheckForEmptyMonoms() {
        if (IsEmpty())
            return;
        for (PTDatLink link = pFirst; link != pHead; ) {
            PTMonom monom = (PTMonom)link->GetDatValue();
            if (monom->GetCoeff() == 0) {
                PTDatLink nextLink = link->GetNextDatLink();
                DelLink(link);
                link = nextLink;
                if (link == pHead)
                    break;
            }
            else 
                link = link->GetNextDatLink();
        }
    }
};
