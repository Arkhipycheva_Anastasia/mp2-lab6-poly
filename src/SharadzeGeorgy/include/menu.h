#ifndef INCLUDE_MENU_H_
#define INCLUDE_MENU_H_

#include "TPolinom.h"
#include <iostream>

using std::cout;
using std::cin;

class Menu {
 private:
	int choice=0;
    TPolinom A;
	TPolinom B;
	int _x, _y, _z = 0;
	char variable;

	void rules() {
		char p = '\0';
        cout << "This is an app for work with polynoms " << endl
            << "Here are the rules: " << endl
            << "1) Polynom is entered without any spaces or specific symbols;" << endl
			<< "2) Format is: ax^0y^0z^0, where \'a\' is a coefficient " << endl
			<< "and zeros are any powers (0<=n<=9). Even the zero power must be printed! " << endl
			<< "Example of the correct polynom: " << endl
			<< "2x^6y^1z^0+5x^2y^0z^0-3x^2y^1z^2" << endl;
		while (p != 'K') {
			cout << "Print \'K\' to continue: ";
			cin >> p;
		}
	}

    void select() {
        cout << "Choose the operation: " << endl
            << "1) Enter the polynom; " << endl
            << "2) Differentiate the polynom; " << endl
            << "3) Integrate the polynom; " << endl
            << "4) Compute; " << endl
            << "5) Make an addition with other polynom; " << endl
            << "6) End the work. " << endl
            << "Your choice is: ";
    }

    void pause() {
        char p = '\0';
        while (p != 'K') {
            cout << "Print \'K\' to continue: ";
            cin >> p;
        }
    }

	void setA() {
		cout << "Enter the polynom: ";
		cin >> A;
	}

	void setB() {
		cout << "Enter the second polynom: ";
		cin >> B;
	}

	void buffer() {
		system("cls");
		cout << "Your polynom is: " << A;
	}

 public:
	
	void start(void){
		rules();
		system("cls");
		setA();

		while (choice != 6){
			buffer();
			select();
			cin >> choice;

			switch (choice){
			case 1:
				setA();
				break;
			case 2:
				cout << "Enter the differentiated variable (x/y/z): ";
				cin >> variable;
				switch (variable){
					case 'x': cout << A.Diff(x); break;
					case 'y': cout << A.Diff(y); break;
					case 'z': cout << A.Diff(z); break;
					default: cout << "Error! " << endl; break;
				}
				break;
			case 3:
				cout << "Enter the integrated variable (x/y/z): ";
				cin >> variable;
				switch (variable){
					case 'x': cout << A.Integrate(x); break;
					case 'y': cout << A.Integrate(y); break;
					case 'z': cout << A.Integrate(z); break;
					default: cout << "Error! " << endl; break;
				}
				break;
			case 4:
				cout << "Enter the x: ";
				cin >> _x;
				cout << "Enter the y: ";
				cin >> _y;
				cout << "Enter the z: ";
				cin >> _z;
				cout << "Value of polynom with the entered variables is: "<< endl
					<< A.Calc(_x, _y, _z);
				break;
			case 5:
				setB();
				cout <<"Sum is: "<< endl
					<< A + B;
				break;
			case 6:
                return;
				break;
			default:
				cout << "Error! " << endl;
				break;
			}
			pause();
		}
	}
};

#endif  // INCLUDE_MENU_H_
