#ifndef INCLUDE_TPOLYNOM_H_
#define INCLUDE_TPOLYNOM_H_

#include "THeadRing.h"
#include <iostream>
#include "TMonom.h"

using namespace std;

enum variables {x = 100, y = 10, z = 1};

class TPolinom : public THeadRing {
public:
	TPolinom(int monoms[][2] = NULL, int km = 0); // �����������
	// �������� �� ������� ������������-������
	TPolinom(TPolinom &q);      // ����������� �����������
	PTMonom  GetMonom()  { return (PTMonom)GetDatValue(); }
	TPolinom  operator+(TPolinom & q); // �������� ���������
	TPolinom & operator=(TPolinom & q); // ������������
	TPolinom Diff(variables xyz = x); // ����������������� �� �������� ����������
	TPolinom Integrate(variables xyz = x); // �������������� �� �������� ����������
	double Calc(int x = 0, int y = 0, int z = 0); // ���������� �������� �������� � ��������� ���������� ����������
	friend ostream & operator<<(ostream & os, TPolinom & q); // ���������� ������
	friend istream & operator>>(istream & os, TPolinom & q); // ���������� �����

};

#endif  // INCLUDE_TPOLYNOM_H_
