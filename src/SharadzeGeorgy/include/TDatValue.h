#ifndef INCLUDE_TDATVALUE_H_
#define INCLUDE_TDATVALUE_H_

class TDatValue;
typedef TDatValue *PTDatValue;

class TDatValue {
public:
	virtual TDatValue * GetCopy() = 0; // �������� �����
	~TDatValue() {}
};

#endif  // INCLUDE_TDATVALUE_H_
