#ifndef INCLUDE_THEADRING_H_
#define INCLUDE_THEADRING_H_

#include "TDatList.h"

class THeadRing : public TDatList {
protected:
	PTDatLink pHead;     // ���������, pFirst - ����� �� pHead
public:
	THeadRing();
	~THeadRing();
	// ������� �������
	virtual void InsFirst(PTDatValue pVal = NULL); // ����� ���������
	// �������� �������
	virtual void DelFirst(void);                 // ������� ������ �����
};

#endif  // INCLUDE_THEADRING_H_
