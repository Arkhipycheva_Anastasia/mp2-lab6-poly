#include "gtest/gtest.h"
#include "TDatList.h"

typedef struct ValueInt:TDatValue {
	int Value;
	ValueInt() { Value = 0; }
	virtual TDatValue * GetCopy() { TDatValue *temp = new ValueInt; return temp; }
} *PValueInt;

class TestList : public ::testing::Test {
protected:
	virtual void SetUp() {
		Val = new ValueInt();
		Val->Value = 100;
 
		ArrVal = new PValueInt[N];
		for (int i = 0; i < N; i++) {
			ArrVal[i] = new ValueInt();
			ArrVal[i]->Value = i;
		}
	}
	virtual void TearDown() {
		delete ArrVal;
	}
	const int N = 10;
	PValueInt Val;
	PValueInt *ArrVal;
	TDatList List;
};

TEST_F(TestList, can_adding_an_item_to_the_end) {
	List.InsLast(Val);
	PValueInt temp = (PValueInt)List.GetDatValue();
	EXPECT_EQ(Val->Value,temp->Value);
}

TEST_F(TestList, size_of_list) {
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	EXPECT_EQ(List.GetListLength(), N);
}

TEST_F(TestList, can_add_ten_elems_to_the_end_on_list) {
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	for (int i = 0; i < N; i++, List.GoNext())
		EXPECT_EQ( ((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value );
}

TEST_F(TestList, can_add_ten_elems_to_the_first_on_list) {
	for (int i = 0; i < N; i++)
		List.InsFirst(ArrVal[i]);
	for (int i = N-1; i > 0; i--, List.GoNext())
		EXPECT_EQ( ((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value );
}

TEST_F(TestList, can_add_ten_elems_to_the_current_on_list) {
	for (int i = 0; i < 5; i++)
		List.InsLast(ArrVal[i]);
	List.GoNext();
	List.GoNext();
	List.InsCurrent(Val);
	List.Reset();

	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[0]->Value);
	List.GoNext();

	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[1]->Value);
	List.GoNext();

	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, Val->Value);
	List.GoNext();

	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[2]->Value);
}

TEST_F(TestList, can_delete_elem_to_the_current_on_list) {
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.GoNext();
	List.DelCurrent();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[2]->Value);
	EXPECT_EQ(List.GetListLength(), N-1);
}

TEST_F(TestList, delete_elem_to_the_first_on_list) {
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.DelFirst();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[1]->Value);
	EXPECT_EQ(List.GetListLength(), N - 1);
}
