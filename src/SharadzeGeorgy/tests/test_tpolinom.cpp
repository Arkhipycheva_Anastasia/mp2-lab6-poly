#include "gtest/gtest.h"
#include "TPolinom.h"

TEST(TPolinom, can_create_pol_from_only_monom) {
	int mon[][2] = { {1, 3} };
	TPolinom A(mon, 1);
	TMonom res(1, 3);
	EXPECT_EQ(res, *A.GetMonom());
}

TEST(TPolinom, can_create_pol_from_two_monoms) {
	const int size = 2;
	int mon[][2] = { {1, 3}, {2, 4} };
	
	TPolinom Pol (mon, size);

	TMonom monoms[size];
	for (int i = 0; i < size; i++)
		monoms[i] = TMonom(mon[i][0], mon[i][1]);
	for (int i = 0; i < size; i++, Pol.GoNext())
		EXPECT_EQ(monoms[i], *Pol.GetMonom());
}

TEST(TPolinom, can_create_pol_from_ten_monoms) {
	const int size = 10;
	int mon[][2] = { { 1, 3 }, { 2, 4 }, { 2, 100 }, { 3, 110 },
	{ 5, 150 }, { 6, 302 }, { 3, 400 }, { 2, 500 }, { 7 ,800 }, { 2, 888 } };
	
	TPolinom Pol(mon, size);
	
	TMonom monoms[size];
	for (int i = 0; i < size; i++)
		monoms[i] = TMonom(mon[i][0], mon[i][1]);
	for (int i = 0; i < size; i++, Pol.GoNext())
		EXPECT_EQ(monoms[i], *Pol.GetMonom());
}

TEST(TPolinom, can_compare_the_polynoms) {
	const int size = 3;
	int mon[][2] = { { 1, 3 }, { 2, 4 }, { 2, 100 } };
	TPolinom Pol1(mon, size);
	TPolinom Pol2(mon, size);

	EXPECT_TRUE(Pol1==Pol2);
}

TEST(TPolinom, can_copy_polinoms) {
	const int size = 2;
	int mon[][2] = { { 5, 3 }, { 2, 4 } };
	TPolinom Pol1(mon, size);

	TPolinom Pol2 = Pol1;

	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, can_assign_polynoms) {
	const int size = 2;
	int mon[][2] = { { 5, 3 }, { 2, 4 } };
	TPolinom Pol1(mon, size);
	TPolinom Pol2;

	Pol2 = Pol1;

	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, can_assign_empty_polynom) {
	TPolinom Pol1;
	TPolinom Pol2;

	Pol2 = Pol1;

	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, can_add_up_linear_polynoms) {
	const int size = 1;
	int mon1[][2] = { { 2, 1 } };
	int mon2[][2] = { { 1, 1 } };
	TPolinom Pol1(mon1, size);
	TPolinom Pol2(mon2, size);

	TPolinom Pol = Pol1 + Pol2;

	const int expected_size = 1;
	int expected_mon[][2] = { { 3, 1 } };
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_add_up_simple_polynoms) {
	const int size1 = 3;
	const int size2 = 4;
	int mon1[][2] = { { 5, 2 }, { 8, 3 }, { 9, 4 } };
	int mon2[][2] = { { 1, 1 }, { -8, 3 }, { 1, 4 }, { 2, 5 } };
	TPolinom Pol1(mon1, size1);
	TPolinom Pol2(mon2, size2);

	TPolinom Pol = Pol1 + Pol2;

	const int expected_size = 4;
	int expected_mon[][2] = { { 1, 1 }, { 5, 2 }, { 10, 4 }, { 2, 5 } };
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_add_up_polynoms) {
	const int size1 = 5;
	const int size2 = 4;
	int mon1[][2] = { { 5, 213 }, { 8, 321 }, { 10, 432 }, { -21, 500 }, { 10, 999 } };
	int mon2[][2] = { { 15, 0 }, { -8, 321 }, { 1, 500 }, { 20, 702 } };
	TPolinom Pol1(mon1, size1);
	TPolinom Pol2(mon2, size2);

	TPolinom Pol = Pol1 + Pol2;

	const int expected_size = 6;
	int expected_mon[][2] = { { 15, 0 }, { 5, 213 }, { 10, 432 }, { -20, 500 }, { 20, 702 }, { 10, 999 } };
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_add_up_many_polynoms) {
	const int size1 = 3;
	const int size2 = 4;
	const int size3 = 3;
	int mon1[][2] = { { 5, 2 }, { 8, 3 }, { 9, 4 } };
	int mon2[][2] = { { 1, 1 }, { -8, 3 }, { 1, 4 }, { 2, 5 } };
	int mon3[][2] = { { 10, 0 }, { 2, 3 }, { 8, 5 } };
	TPolinom Pol1(mon1, size1);
	TPolinom Pol2(mon2, size2);
	TPolinom Pol3(mon3, size3);

	TPolinom Pol = Pol1 + Pol2 + Pol3;

	const int expected_size = 6;
	int expected_mon[][2] = { { 10, 0 }, { 1, 1 }, { 5, 2 }, { 2, 3 }, { 10, 4 }, { 10, 5 } };
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_take_derivative_of_simple_polynoms) {
	const int size = 3;
	int mon[][2] = { { 5, 2 }, { 8, 3 }, { 9, 4 } };
	TPolinom Pol(mon, size);

	TPolinom derivative_Pol = Pol.Derivative( z );

	const int expected_size = 3;
	int expected_mon[][2] = { { 10, 1 }, { 24, 2 }, { 36, 3 } };
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(derivative_Pol == expected_Pol);
}

TEST(TPolinom, can_take_derivatives_of_complex_polynoms_over_the_variable_x) {
	const int size = 4;
	int mon[][2] = { { 15, 0 }, { -8, 321 }, { 1, 500 }, { 20, 702 } };
	TPolinom Pol(mon, size);

	TPolinom derivative_Pol = Pol.Derivative(x);

	const int expected_size = 3;
	int expected_mon[][2] = { { -24, 221 }, { 5, 400 }, { 140, 602 } };
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(derivative_Pol == expected_Pol);
}

TEST(TPolinom, can_take_derivatives_of_complex_polynoms_over_the_variable_y) {
	const int size = 4;
	int mon[][2] = { { 15, 0 }, { -8, 321 }, { 1, 500 }, { 20, 702 } };
	TPolinom Pol(mon, size);

	TPolinom derivative_Pol = Pol.Derivative(y);

	const int expected_size = 1;
	int expected_mon[][2] = { { -16, 311 } };
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(derivative_Pol == expected_Pol);
}

TEST(TPolinom, can_take_derivatives_of_complex_polynoms_over_the_variable_z) {
	const int size = 4;
	int mon[][2] = { { 15, 0 }, { -8, 321 }, { 1, 500 }, { 20, 702 } };
	TPolinom Pol(mon, size);

	TPolinom derivative_Pol = Pol.Derivative(z);

	const int expected_size = 2;
	int expected_mon[][2] = { { -8, 320 }, { 40, 701 } };
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(derivative_Pol == expected_Pol);
}