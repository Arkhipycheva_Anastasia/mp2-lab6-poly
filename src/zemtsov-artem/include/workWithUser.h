//
//  workWithUser.h
//  polynoms
//
//  Created by артем on 14.04.16.
//  Copyright © 2016 артем. All rights reserved.
//
#pragma once

#include "Polinom.h"

class Menu {
private:
    int choise=0;
    TPolinom A;
    TPolinom B;
    int _x, _y, _z = 0;
    char variable;
    
    void rules(void){
        char mindgames = 'N';
        cout << "Здравствуйте, это приложение позволяет Вам производить работу с полиномами. " << endl
        << "Для более корректной работы, пожалуйста, ознакомьтесь с основными " << endl
        << "правилами ввода полинома: " << endl
        << "1)Полином вводится без пробелов и без лишних символов;" << endl
        << "2)Формат ввода каждого монома такой: ax^0y^0z^0, где a-коэффициент, " << endl
        << "а вместо нулей могут стоять любые степени (0<=n<=9). Важно отметить, что даже" << endl
        << "нулевую степень прописывать обязательно. " << endl
        << "Пример корректно введенного полинома: " << endl
        << "3x^2y^0z^3-12x^1y^1z^3+4x^3y^2z^1" << endl;
        while (mindgames != 'Y'){
            cout << "Вы внимательно ознакомились с правилами? (Y/N) ";
            cin >> mindgames;
        }
    }
    
    void setA(void) {
        cout << "Введите полином: ";
        cin >> A;
    }
    
    void setB(void) {
        cout << "Введите второй полином: ";
        cin >> B;
    }
    
    void buffer(void){
        system("cls");
        cout << "Ваш полином: " << A;
    }
    
    void pause(void){
        char p = 'N';
        while (p != 'Y'){
            cout << "Продолжить работу?(Y/N) ";
            cin >> p;
        }
        
        
    }
    
    void selection(void){
        cout << "Выберите, что Вы хотите сделать: " << endl
        << "1) Ввести полином заново;" << endl
        << "2) Продифференцировать полином;" << endl
        << "3) Проинтегрировать полином;" << endl
        << "4) Вычислить значение;" << endl
        << "5) Сложить с другим полиномом;" << endl
        << "6) Завершить работу." << endl
        << "Ваш выбор: ";
    }
    
public:
    
    void start(void){
        rules();
        system("cls");
        setA();
        while (choise != 6){
            buffer();
            selection();
            cin >> choise;
            switch (choise){
                case 1:
                    setA();
                    break;
                case 2:
                    cout << "Введите, по какой переменной дифференцируем(x/y/z): ";
                    cin >> variable;
                    switch (variable){
                        case 'x': cout << A.Diff(x); break;
                        case 'y': cout << A.Diff(y); break;
                        case 'z': cout << A.Diff(z); break;
                        default: cout << "Error!" << endl; break;
                    }
                    break;
                case 3:
                    cout << "Введите, по какой переменной интегрируем(x/y/z): ";
                    cin >> variable;
                    switch (variable){
                        case 'x': cout << A.Integr(x); break;
                        case 'y': cout << A.Integr(y); break;
                        case 'z': cout << A.Integr(z); break;
                        default: cout << "Error!" << endl; break;
                    }
                    break;
                case 4:
                    cout << "Введите x: ";
                    cin >> _x;
                    cout << "Введите y: ";
                    cin >> _y;
                    cout << "Введите z: ";
                    cin >> _z;
                    cout << "Значение полинома при заданных значениях переменных = "<< endl
                    << A.Calc(_x, _y, _z);
                    break;
                case 5:
                    setB();
                    cout <<"Сумма полиномов: "<< endl
                    << A + B;
                    break;
                case 6:
                    cout << "Всего доброго!" << endl;
                    break;
                default:
                    cout << "Error! " << endl;
                    break;
            }
            pause();
        }
    }
    
};