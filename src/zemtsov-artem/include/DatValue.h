//
//  DatValue.h
//  polynoms
//
//  Created by артем on 10.04.16.
//  Copyright © 2016 артем. All rights reserved.
//
#pragma once

class TDatValue;
typedef TDatValue *PTDatValue;

class TDatValue {
public:
    virtual TDatValue * GetCopy() = 0; // создание копии
    virtual ~TDatValue() {}
};