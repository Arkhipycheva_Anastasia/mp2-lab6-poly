#pragma once

#include "DatValue.h"

class TMonom;
typedef TMonom *PTMonom;

class TMonom : public TDatValue  {
public:
	TMonom(int cval = 1, int ival = 0) {
		Coeff = cval; Index = ival;
	}

	virtual TDatValue * GetCopy() { 
        TDatValue * temp = new TMonom(Coeff, Index); 
        return temp;
    }

	void    SetCoeff(int cval)  { Coeff = cval; }
	int     GetCoeff(void)      { return Coeff; }
	void    SetIndex(int ival)  { Index = ival; }
	int     GetIndex(void)      { return Index; }

	TMonom& operator=(const TMonom &tm) {
		Coeff = tm.Coeff;
        Index = tm.Index;
		return *this;
	}

	bool operator==(const TMonom &tm) const {
		return (Coeff == tm.Coeff) && (Index == tm.Index);
	}

    bool operator!=(const TMonom &tm) const {
        return (!operator==(tm));
    }

	int operator<(const TMonom &tm) const {
		return Index < tm.Index;
	}

	friend class TPolinom;

protected:
    int Coeff;  // коэффициент монома
    int Index;  // индекс (свертка степеней)

};