#pragma once

#include <iostream>

#include "HeadRing.h"
#include "Monom.h"

using std::string;

enum Variables { x, y, z };

class TPolinom : public THeadRing {
public:
    TPolinom(int monoms[][2] = nullptr, int km = 0); // конструктор
                               // полинома из массива «коэффициент-индекс»
    TPolinom(TPolinom &q);

    PTMonom GetMonom() { return (PTMonom)GetDatValue(); }

    TPolinom    operator+   (TPolinom &q);
    TPolinom&   operator=   (TPolinom &q);
    bool        operator==  (TPolinom &q);
    bool        operator!=  (TPolinom &q) { return (!operator==(q)); }

    TPolinom    Diff       (Variables var = x);
    TPolinom    Integral   (Variables var = x);

    double Calc(double x = 0.0, double y = 0.0, double z = 0.0);
};
