#pragma once

#include "RootLink.h"

class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink {
public:
	TDatLink(PTDatValue pVal = nullptr, PTRootLink pN = nullptr) : TRootLink(pN) 
        { pValue = pVal; }

	void       SetDatValue(PTDatValue pVal)     { pValue = pVal; }
	PTDatValue GetDatValue()                    { return  pValue; }
	PTDatLink  GetNextDatLink()                 { return  (PTDatLink)pNext; }

	friend class TDatList;

protected:
    PTDatValue pValue;  // указатель на объект значения
};
