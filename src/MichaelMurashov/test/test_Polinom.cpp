#include "gtest/gtest.h"
#include "Polinom.h"

TEST(TPolinom, can_create_pol_from_only_monom)
{
    // Arrange
    int mon[][2] = { { 1, 3 } };

    // Act
    TPolinom A(mon, 1);

    // Assert
    TMonom res(1, 3);
    EXPECT_EQ(res, *A.GetMonom());
}

TEST(TPolinom, can_create_pol_from_two_monoms)
{
    // Arrange
    const int size = 2;
    int mon[][2] = { { 1, 3 },{ 2, 4 } };

    // Act
    TPolinom Pol(mon, size);

    // Assert
    TMonom monoms[size];
    for (int i = 0; i < size; i++)
        monoms[i] = TMonom(mon[i][0], mon[i][1]);
    for (int i = 0; i < size; i++, Pol.GoNext())
        EXPECT_EQ(monoms[i], *Pol.GetMonom());
}

TEST(TPolinom, can_create_pol_from_ten_monoms)
{
    // Arrange
    const int size = 10;
    int mon[][2] = { { 1, 3 },{ 2, 4 },{ 2, 100 },{ 3, 110 },
    { 5, 150 },{ 6, 302 },{ 3, 400 },{ 2, 500 },{ 7 ,800 },{ 2, 888 } };

    // Act
    TPolinom Pol(mon, size);

    // Assert
    TMonom monoms[size];
    for (int i = 0; i < size; i++)
        monoms[i] = TMonom(mon[i][0], mon[i][1]);
    for (int i = 0; i < size; i++, Pol.GoNext())
        EXPECT_EQ(monoms[i], *Pol.GetMonom());
}

TEST(TPolinom, can_compare_the_polynoms)
{
    // Arrange
    const int size = 3;
    int mon[][2] = { { 1, 3 },{ 2, 4 },{ 2, 100 } };
    TPolinom Pol1(mon, size);
    TPolinom Pol2(mon, size);

    // Act & Assert
    EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, can_copy_polinoms)
{
    // Arrange
    const int size = 2;
    int mon[][2] = { { 5, 3 },{ 2, 4 } };
    TPolinom Pol1(mon, size);

    // Act
    TPolinom Pol2 = Pol1;

    // Assert
    EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, can_assign_polynoms)
{
    // Arrange
    const int size = 2;
    int mon[][2] = { { 5, 3 },{ 2, 4 } };
    TPolinom Pol1(mon, size);
    TPolinom Pol2;

    // Act
    Pol2 = Pol1;

    // Assert
    EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, can_assign_empty_polynom)
{
    // Arrange
    TPolinom Pol1;
    TPolinom Pol2;

    // Act
    Pol2 = Pol1;

    // Assert
    EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, can_add_up_linear_polynoms)
{
    // Arrange
    const int size = 1;
    int mon1[][2] = { { 2, 1 } };
    int mon2[][2] = { { 1, 1 } };
    // 2z
    TPolinom Pol1(mon1, size);
    // z
    TPolinom Pol2(mon2, size);

    // Act
    TPolinom Pol = Pol1 + Pol2;

    // Assert
    const int expected_size = 1;
    int expected_mon[][2] = { { 3, 1 } };
    // z+2z^2
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_add_up_simple_polynoms)
{
    // Arrange
    const int size1 = 3;
    const int size2 = 4;
    int mon1[][2] = { { 5, 2 },{ 8, 3 },{ 9, 4 } };
    int mon2[][2] = { { 1, 1 },{ -8, 3 },{ 1, 4 },{ 2, 5 } };
    // 5z^2+8z^3+9z^4
    TPolinom Pol1(mon1, size1);
    // z-8z^3+z^4+2z^5
    TPolinom Pol2(mon2, size2);

    // Act
    TPolinom Pol = Pol1 + Pol2;

    // Assert
    const int expected_size = 4;
    int expected_mon[][2] = { { 1, 1 },{ 5, 2 },{ 10, 4 },{ 2, 5 } };
    // z+5z^2+10z^4+2z^5
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_add_up_polynoms)
{
    // Arrange
    const int size1 = 5;
    const int size2 = 4;
    int mon1[][2] = { { 5, 213 },{ 8, 321 },{ 10, 432 },{ -21, 500 },{ 10, 999 } };
    int mon2[][2] = { { 15, 0 },{ -8, 321 },{ 1, 500 },{ 20, 702 } };
    // 5x^2yz^3+8x^3y^2z+10x^4y^3z^2-21x^5+10x^9y^9z^9
    TPolinom Pol1(mon1, size1);
    // 15-8x^3y^2z+x^5+20x^7z^2
    TPolinom Pol2(mon2, size2);

    // Act
    TPolinom Pol = Pol1 + Pol2;

    // Assert
    const int expected_size = 6;
    int expected_mon[][2] = { { 15, 0 },{ 5, 213 },{ 10, 432 },{ -20, 500 },{ 20, 702 },{ 10, 999 } };
    // 15+5x^2yz^3+10x^4y^3z^2-20x^5+20x^7z^2+10x^9y^9z^9
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_add_up_many_polynoms)
{
    // Arrange
    const int size1 = 3;
    const int size2 = 4;
    const int size3 = 3;
    int mon1[][2] = { { 5, 2 },{ 8, 3 },{ 9, 4 } };
    int mon2[][2] = { { 1, 1 },{ -8, 3 },{ 1, 4 },{ 2, 5 } };
    int mon3[][2] = { { 10, 0 },{ 2, 3 },{ 8, 5 } };
    // 5z^2+8z^3+9z^4
    TPolinom Pol1(mon1, size1);
    // z-8z^3+z^4+2z^5
    TPolinom Pol2(mon2, size2);
    // 10+2z^3+8z^5
    TPolinom Pol3(mon3, size3);

    // Act
    TPolinom Pol = Pol1 + Pol2 + Pol3;

    // Assert
    const int expected_size = 6;
    int expected_mon[][2] = { { 10, 0 },{ 1, 1 },{ 5, 2 },{ 2, 3 },{ 10, 4 },{ 10, 5 } };
    // z+5z^2+10z^4+2z^5
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_take_derivative_of_simple_polynoms)
{
    // Arrange
    const int size = 3;
    int mon[][2] = { { 5, 2 },{ 8, 3 },{ 9, 4 } };
    // 5z^2+8z^3+9z^4
    TPolinom Pol(mon, size);

    // Act
    TPolinom diff_Pol = Pol.Diff(z);

    // Assert
    const int expected_size = 3;
    int expected_mon[][2] = { { 10, 1 },{ 24, 2 },{ 36, 3 } };
    // 10z24z^2+36z^3
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(diff_Pol == expected_Pol);
}

TEST(TPolinom, can_take_derivatives_of_complex_polynoms_over_the_variable_x)
{
    // Arrange
    const int size = 4;
    int mon[][2] = { { 15, 0 },{ -8, 321 },{ 1, 500 },{ 20, 702 } };
    // 15-8x^3y^2z+x^5+20x^7z^2
    TPolinom Pol(mon, size);

    // Act
    TPolinom diff_Pol = Pol.Diff(x);

    // Assert
    const int expected_size = 3;
    int expected_mon[][2] = { { -24, 221 },{ 5, 400 },{ 140, 602 } };
    // -24x^2y^2z + 5x^4 + 140x^6z^2
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(diff_Pol == expected_Pol);
}

TEST(TPolinom, can_take_derivatives_of_complex_polynoms_over_the_variable_y)
{
    // Arrange
    const int size = 4;
    int mon[][2] = { { 15, 0 },{ -8, 321 },{ 1, 500 },{ 20, 702 } };
    // 15-8x^3y^2z+x^5+20x^7z^2
    TPolinom Pol(mon, size);

    // Act
    TPolinom diff_Pol = Pol.Diff(y);

    // Assert
    const int expected_size = 1;
    int expected_mon[][2] = { { -16, 311 } };
    // -16x^3yz
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(diff_Pol == expected_Pol);
}

TEST(TPolinom, can_take_derivatives_of_complex_polynoms_over_the_variable_z)
{
    // Arrange
    const int size = 4;
    int mon[][2] = { { 15, 0 },{ -8, 321 },{ 1, 500 },{ 20, 702 } };
    // 15-8x^3y^2z+x^5+20x^7z^2
    TPolinom Pol(mon, size);

    // Act
    TPolinom diff_Pol = Pol.Diff(z);

    // Assert
    const int expected_size = 2;
    int expected_mon[][2] = { { -8, 320 },{ 40, 701 } };
    // -8x^3y^2+7x^3z
    TPolinom expected_Pol(expected_mon, expected_size);
    EXPECT_TRUE(diff_Pol == expected_Pol);
}

TEST(TPolinom, can_take_integral_of_polynom)
{
    // Arrange
    int mon[][2] = { { 9, 2 },{ 8, 3 },{ 10, 4 } };
    // 9z^2 + 8z^3 + 10z^4
    TPolinom Pol(mon, 3);

    // Act
    TPolinom integral_Pol = Pol.Integral(z);

    // Assert
    int expected_mon[][2] = { { 3, 3 },{ 2, 4 },{ 2, 5 } };
    // 3z^3 + 2z^4 + 2z^5
    TPolinom expected_Pol(expected_mon, 3);
    EXPECT_TRUE(integral_Pol == expected_Pol);
}

TEST(TPolinom, check_calculating)
{
    // Arrange
    int mon[][2] = { { 15, 300 },{ 1, 20 },{ 4, 201 } };
    // 15*x^3 + y^2 + 4*x^2*z
    TPolinom Pol(mon, 3);

    double x = 2.0;
    double y = 3.0;
    double z = 0.0;

    // Act
    double result = Pol.Calc(x, y, z);

    // Assert
    double extented_value = 15*8 + 9 + 0;
    EXPECT_EQ(result, extented_value);
}
