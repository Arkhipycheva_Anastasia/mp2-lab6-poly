#include <gtest\gtest.h>

#include "../include/DatList.h"

typedef struct ValueInt :TDatValue
{
    int Value;
    ValueInt() { Value = 0; }
    virtual TDatValue * GetCopy() { 
        TDatValue *temp = new ValueInt; 
        return temp; 
    }
} *PValueInt;

// Initialization
class TestList : public ::testing::Test
{
protected:
    virtual void SetUp()
    {
        // Testing only elem
        Val = new ValueInt();
        Val->Value = 100;

        // Testing ten elems 
        ArrVal = new PValueInt[N];
        for (int i = 0; i < N; i++) {
            ArrVal[i] = new ValueInt();
            ArrVal[i]->Value = i;
        }
    }

    virtual void TearDown()
    {
        delete ArrVal;
    }

    // Arrange
    const int N = 10;
    PValueInt Val;
    PValueInt *ArrVal;
    TDatList list;
};

TEST_F(TestList, can_insert_first_in_empty_list) {
    list.InsFirst(Val);

    EXPECT_EQ(list.GetDatValue(), Val);
}

TEST_F(TestList, can_insert_first_in_not_empty_list) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    list.InsFirst(Val);

    EXPECT_EQ(list.GetDatValue(FIRST), Val);
}

TEST_F(TestList, can_insert_last_in_empty_list) {
    list.InsLast(Val);

    EXPECT_EQ(list.GetDatValue(), Val);
}

TEST_F(TestList, can_insert_last_in_not_empty_list) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    list.InsLast(Val);

    EXPECT_EQ(list.GetDatValue(LAST), Val);
}

TEST_F(TestList, can_insert_current) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    list.SetCurrentPos(N - 3);
    list.InsCurrent(Val);

    EXPECT_EQ(list.GetDatValue(CURRENT), Val);
}

TEST_F(TestList, can_get_value_first) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    EXPECT_EQ(list.GetDatValue(FIRST), ArrVal[0]);
}

TEST_F(TestList, can_get_value_current) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    list.SetCurrentPos(N - 3);

    EXPECT_EQ(list.GetDatValue(CURRENT), ArrVal[N -3]);
}

TEST_F(TestList, can_get_value_last) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    EXPECT_EQ(list.GetDatValue(LAST), ArrVal[N - 1]);
}

TEST_F(TestList, can_set_and_get_current_pos) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    list.SetCurrentPos(N - 3);

    EXPECT_EQ(list.GetCurrentPos(), N - 3);
}

TEST_F(TestList, error_when_set_current_pos_less_zero) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    EXPECT_EQ(list.SetCurrentPos(-1), ERROR);
}

TEST_F(TestList, error_when_current_pos_more_list_length) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    EXPECT_EQ(list.SetCurrentPos(N + 5), ERROR);
}

TEST_F(TestList, can_reset_not_empty_list) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    EXPECT_EQ(list.Reset(), OK);
    EXPECT_EQ(list.GetCurrentPos(), 0);
}

TEST_F(TestList, error_when_reset_empty_list) {
    EXPECT_EQ(list.Reset(), ERROR);
}

TEST_F(TestList, check_ended_of_list_in_end) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    list.SetCurrentPos(N);

    EXPECT_TRUE(list.IsListEnded());
}

TEST_F(TestList, check_ended_of_list_in_middle) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    list.SetCurrentPos(N - 5);

    EXPECT_FALSE(list.IsListEnded());
}

TEST_F(TestList, can_go_next) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    list.SetCurrentPos(N - 5);
    list.GoNext();

    EXPECT_EQ(list.GetCurrentPos(), N - 4);
}

TEST_F(TestList, error_when_go_next_in_end_of_list) {
    EXPECT_EQ(list.GoNext(), ERROR);
}

TEST_F(TestList, can_del_first) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    list.DelFirst();

    EXPECT_EQ(list.GetDatValue(FIRST), ArrVal[1]);
}

TEST_F(TestList, can_del_first_when_one_elem) {
    list.InsLast(Val);

    list.DelFirst();

    EXPECT_TRUE(list.IsEmpty());
}

TEST_F(TestList, can_del_first_when_currpos_on_first) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    list.SetCurrentPos(0);

    list.DelFirst();

    EXPECT_EQ(list.GetDatValue(CURRENT), ArrVal[1]);
}

TEST_F(TestList, can_del_current) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    list.SetCurrentPos(2);
    list.DelCurrent();
    
    EXPECT_EQ(list.GetDatValue(CURRENT), ArrVal[3]);
}

TEST_F(TestList, can_del_all_list) {
    for (int i = 0; i < N; i++)
        list.InsLast(ArrVal[i]);

    list.DelList();

    EXPECT_EQ(list.GetListLength(), 0);
}