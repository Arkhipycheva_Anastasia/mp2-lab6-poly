#pragma once
#include "HeadRing.h"
#include "Monom.h"
#include <string>
#include <iostream>
#include <regex>
#include "boost\lexical_cast.hpp"

using namespace std;
using boost::lexical_cast;

class TPolinom : public THeadRing 
{
public:
    TPolinom(int monoms[][2] = NULL, int km = 0); // �����������
                                                  // �������� �� ������� ������������-������
    TPolinom(TPolinom &q);      // ����������� �����������
    PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }
    TPolinom operator+(TPolinom &q); // �������� ���������
    TPolinom & operator=(TPolinom &q); // ������������

    //���� ������
    bool operator==(TPolinom &q);
    bool operator!=(TPolinom &q)
    { return (!operator==(q)); }
    double Calc(double x = 0.0, double y = 0.0, double z = 0.0);

    friend ostream & operator<<(ostream &os, TPolinom &q);	// �����
    friend istream & operator>>(istream &os, TPolinom &q);	// ����
};
