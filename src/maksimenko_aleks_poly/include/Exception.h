#pragma once
#include <exception>

class pendingTheInput : public std::exception {
public:
 char * what() {
        return "Pending the input of the polynomial.";
    }

};

class notEqualTheNumber : public std::exception {
public:
 char * what() {
        return "The ratio of monoma not equal to the number.";
    }
};

class theExtentNotIncluded : public std::exception {
public:
 char * what() {
        return "the extent not included in the interval [0;9]";
    }
};

class notMatchTheNumbers : public std::exception {
public:
 char * what() {
        return "n the line there are elements that do not match the numbers";
    }
};