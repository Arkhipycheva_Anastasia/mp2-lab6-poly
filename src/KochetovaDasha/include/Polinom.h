﻿#pragma once
#include "HeadRing.h"
#include <iostream>
#include "Monom.h"

using namespace std;

enum variables {x=100,y=10,z=1};

class TPolinom : public THeadRing {
public:
    TPolinom(int monoms[][2] = nullptr, int km = 0); // конструктор
    // полинома из массива «коэффициент-индекс»
    TPolinom(TPolinom &q);      // конструктор копирования
    PTMonom  GetMonom()  { return (PTMonom)GetDatValue(); }
    TPolinom  operator+(TPolinom &q); // сложение полиномов
    TPolinom & operator=(TPolinom &q); // присваивание
    double Calc(int x = 0, int y = 0, int z = 0); // вычисление значения полинома с заданными значениями переменных
    friend ostream & operator<<(ostream & os, TPolinom & q); // перегрузка вывода
    friend istream & operator>>(istream & os, TPolinom & q); // перегрузка ввода
};