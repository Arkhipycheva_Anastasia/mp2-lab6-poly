﻿#pragma once
#include "DatList.h"

class THeadRing : public TDatList{
protected:
    PTDatLink pHead;     // заголовок, pFirst - звено за pHead
public:
    THeadRing();
    ~THeadRing();
    virtual void InsFirst(PTDatValue pVal = nullptr); // после заголовка
    virtual void DelFirst(void);                 // удалить первое звено
};