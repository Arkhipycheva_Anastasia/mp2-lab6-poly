#pragma once
#include "Polinom.h"

class Menu {
private:
    int choise=0;
    TPolinom A;
    TPolinom B;
    int _x, _y, _z = 0;
    char variable;
    
    void rules(void){
        cout << "Здравствуйте, с помощью этого приложения вы сможете работать с полиномами. " << endl
        << "Пожалуйста, ознакомьтесь с основными правилами ввода полинома:" << endl
        << "1)Полином вводится без пробелов и без лишних символов;" << endl
        << "2)Формат ввода каждого монома такой: ax^0y^0z^0, где a-коэффициент, " << endl
        << "а вместо нулей могут стоять любые степени (0<=n<=9). Важно отметить, что даже" << endl
        << "нулевую степень прописывать обязательно. " << endl
        << "Пример корректно введенного полинома: " << endl
        << "3x^2y^0z^3-12x^1y^1z^3+4x^3y^2z^1" << endl;
    }
    
    void setA(void) {
        cout << "Введите полином: ";
        cin >> A;
    }
    
    void setB(void) {
        cout << "Введите второй полином: ";
        cin >> B;
    }
    
    void buffer(void){
        system("cls");
        cout << "Ваш полином: " << A;
    }
    
    void pause(void){
        char p = 'N';
        while (p != 'Y'){
            cout << "Продолжить работу?(Y/N) ";
            cin >> p;
        }
    }
    
    void selection(void){
        cout << "Выберите, что Вы хотите сделать: " << endl
        << "1) Ввести полином заново;" << endl
        << "2) Вычислить значение;" << endl
        << "3) Сложить с другим полиномом;" << endl
        << "4) Завершить работу." << endl
        << "Ваш выбор: ";
    }
    
public:
    void start(void){
        rules();
        setA();
        while (choise != 4){
            buffer();
            selection();
            cin >> choise;
            switch (choise){
                case 1:
                    setA();
                    break;
                case 2:
                    cout << "Введите x: ";
                    cin >> _x;
                    cout << "Введите y: ";
                    cin >> _y;
                    cout << "Введите z: ";
                    cin >> _z;
                    cout << "Значение полинома при заданных значениях переменных = "<< endl
                    << A.Calc(_x, _y, _z);
                    break;
                case 3:
                    setB();
                    cout <<"Сумма полиномов: "<< endl
                    << A + B;
                    break;
                case 4:
                    cout << "До встречи!" << endl;
                    break;
                default:
                    cout << "Error! " << endl;
                    break;
            }
            pause();
        }
    }  
};