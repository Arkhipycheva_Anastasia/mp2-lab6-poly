#include "menu.h"
#include <locale>
#include <iostream>

using namespace std;

int main() {
    setlocale(LC_ALL, "rus");
    Menu main_menu;
	main_menu.start();
    system("pause");
	return 0;
}