#include "Polinom.h"
#include "Monom.h"
#include <iostream>
#include <string>
#include <regex>
#include <math.h>

using namespace std;

TPolinom::TPolinom(int monoms[][2], int km) {
    PTMonom elem = new TMonom(0, -1);
    pHead->SetDatValue(elem);
    for (int i = 0; i < km; i++) {
        elem = new TMonom(monoms[i][0], monoms[i][1]);
        InsLast(elem);
    }
}

TPolinom::TPolinom(TPolinom &q) {
    PTMonom elem = new TMonom(0, -1);
    pHead->SetDatValue(elem);
    for (q.Reset(); !q.IsListEnded(); q.GoNext()) {
        elem = q.GetMonom();
        InsLast(elem->GetCopy());
    }
    q.Reset();
}

TPolinom TPolinom::operator+(TPolinom &q) {
    TPolinom result;
    
    Reset();
    q.Reset();
    
    TMonom nul(0, -1);
    TMonom elem1(0,0);
    TMonom elem2(0,0);
    PTMonom tmp = nullptr;
    
    while ((!IsListEnded()) || (!q.IsListEnded())) {
        if (IsListEnded())
        {
            elem1 = nul;
        } else {
            elem1 = *GetMonom();
        }
        if (q.IsListEnded()) {
            elem2 = nul;
        } else {
            elem2 = *q.GetMonom();
        }
        
        if (elem1 < elem2) {
            tmp = new TMonom(elem2.GetCoeff(), elem2.GetIndex());
            q.GoNext();
        } else if (elem2 < elem1) {
            tmp = new TMonom(elem1.GetCoeff(), elem1.GetIndex());
            GoNext();
        }else if (elem1.GetIndex() == elem2.GetIndex()) {
            tmp = new TMonom(elem1.GetCoeff() + elem2.GetCoeff(), elem1.GetIndex());
            GoNext();
            q.GoNext();
        }
        
        if (tmp->GetCoeff()) {
            result.InsLast(tmp);
        }
    }
    return result;
}

TPolinom & TPolinom:: operator=(TPolinom &q) {
    DelList();
    if ((&q) && (&q != this)) {
        PTMonom Mon = new TMonom(0, -1);
        pHead->SetDatValue(Mon);
        for (q.Reset(); !q.IsListEnded(); q.GoNext()) {
            Mon = q.GetMonom();
            InsLast(Mon->GetCopy());
        }
    }
    return *this;
}

double TPolinom::Calc(int x, int y, int z){
    double result = 0;
    int coef, ind = 0;
    int indx, indy, indz = 0;
    for (Reset(); !IsListEnded(); GoNext()){
        coef = GetMonom()->GetCoeff();
        ind = GetMonom()->GetIndex();
        indx = ind / 100;
        indy = (ind % 100) / 10;
        indz = ind % 10;
        result += coef*pow(x, indx)*pow(y, indy)*pow(z, indz);
    }
    return result;
}

ostream & operator<<(ostream & os, TPolinom & q){
    
    string p_out = "";
    string buf = "";
    string sign;
    
    int coef, ind, indx, indy, indz = 0;
    
    for (q.Reset(); !q.IsListEnded(); q.GoNext()){
        coef = q.GetMonom()->GetCoeff();
        ind = q.GetMonom()->GetIndex();
        indx = ind / 100;
        indy = (ind % 100) / 10;
        indz = ind % 10;
        
        sign = "+";
        if (coef < 0)
            sign = "-";
        buf = to_string(abs(coef));
        
        if (indx) {
            if (indx != 1) {
                buf = buf + "x^" + to_string(indx);
            } else {
                buf = buf + "x";
            }
        }
        if (indy) {
            if (indy != 1) {
                buf = buf + "y^" + to_string(indy);
            } else {
                buf = buf + "y";
            }
        }
        if (indz) {
            if (indz != 1) {
                buf = buf + "z^" + to_string(indz);
            } else {
                buf = buf + "z";
            }
        }
        
        if (p_out != "") {
            p_out = p_out + " " + sign + " " + buf;
        } else {
            if (sign == "-") {
                p_out += sign;
            }
            p_out += buf;
        }
    }
    os << p_out << endl;;
    return os;
}




typedef regex_iterator<string::iterator> inter;

istream & operator>>(istream & os, TPolinom & q)
{
    // ��������������� ����������
    int i = 0,j = 0, coeff = 0, index;
    string buf;
    PTMonom mon;
    int count = 0;
    bool inmon = false;

    // ���� "��������"
    os >> buf;

    while (i < strlen(buf.c_str()))
    {
        if (!inmon)   //  ����������� �����������
        {
            while (buf[i + j] >= 48 && buf[i + j] <= 57)
                j++;
            for (int k = 0; k < j; k++)
                coeff += atoi(((const char*)buf[i + k]))*pow(10, k);
            inmon = true;
            i += j;
        }
        if (inmon)    //����������� ������
        {
            index = atoi((const char*)buf[i + 2]) * 100 +  //x
                atoi((const char*)buf[i + 5]) * 10 +       //y
                atoi((const char*)buf[i + 8]);             //z
            inmon = false;
            i += 9;
        }
                                                                  // ������ ����� � ������� � ����� ������
        mon = new TMonom(coeff, index);
        q.InsLast(mon);
    }
    return os;
}