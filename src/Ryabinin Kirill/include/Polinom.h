#pragma once

#include "HeadRing.h"
#include "Monom.h"
#include <iostream>
#include <string>
#include <boost>
#include <regex>
#include <exception>

using namespace std;
using boost::lexical_cast;

using std::string;

enum Variables { x, y, z };

class TPolinom : public THeadRing {
public:
    TPolinom(int monoms[][2] = nullptr, int km = 0); // �����������
                                                     // �������� �� ������� ������������-������
    TPolinom(TPolinom &q);
    PTMonom GetMonom() { return (PTMonom)GetDatValue(); }
    TPolinom    operator+   (TPolinom &q);
    TPolinom&   operator=   (TPolinom &q);
    //�������������� ������
    bool        operator==  (TPolinom &q);
    TPolinom    Diff(Variables xyz = x); //��������������
    TPolinom    Integral(Variables xyz = x); //�����������������
    friend ostream & operator<<(ostream &os, TPolinom &q);	// �����
    friend istream & operator>>(istream &os, TPolinom &q);	// ����
};